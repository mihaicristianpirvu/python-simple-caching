# This is a simple example of caching access to a video stream. We'll just index the frame timestamp.

from pathlib import Path
import sys
from datetime import datetime
from simple_caching import cache_fn
from simple_caching.storage import NpyFS
from media_processing_lib.video import MPLVideo, video_read
import numpy as np

def create_video(n_frames: int):
    video_path = f"video_{n_frames}.mp4"
    print(f"Creating video {video_path}")
    data = np.random.randint(0, 255, size=(n_frames, 640, 480, 3)).astype(np.uint8)
    video = MPLVideo(data, fps=30)
    video.save(video_path)

@cache_fn(NpyFS(f".cache/access_{int(sys.argv[1])}", key_encode_fn=lambda video, t: t))
def access(video: MPLVideo, t: int):
    return video[t]

def main():
    assert len(sys.argv) == 2 and int(sys.argv[1]) > 0, "Usage: python main.py N\n\t -N: number of frames of the video"
    n_frames = int(sys.argv[1])
    video_path = Path(f"video_{n_frames}.mp4")
    if not video_path.exists():
        create_video(n_frames)
    video = video_read(video_path, video_lib="pims")

    k = 0
    while True:
        ix = np.random.randint(n_frames)
        start = datetime.now()
        x = access(video, ix)
        print(f"ix: {ix}. duration: {datetime.now() - start}. shape: {x.shape}. mean: {x.mean()}")
        k += 1
        if k > n_frames * 100:
            break

if __name__ == "__main__":
    main()
