import numpy as np
import time
from simple_caching.storage import NpyFS
from simple_caching import cache_fn
from datetime import datetime

def key_encode_fn(x, y):
    return f"{x.mean()}_{x.std()}"

@cache_fn(NpyFS, key_encode_fn)
def f(x, y):
    time.sleep(1)
    return (x ** 2 / np.sqrt(x)) if np.sqrt(x).mean() > 2.2 else x * 0

def do_cache(data, N, K):
    k = 0
    while True:
        k += 1
        ix = np.random.choice(N)
        now = datetime.now()
        item = data[ix]
        res = f(item, item)
        print(f"Ix {ix}. Took {datetime.now() - now}")
        time.sleep(0.1)
        if k == K:
            break

def main():
    N = 10
    K = 50
    np.random.seed(42)
    data = np.random.randn(N, 420, 420, 3).astype(np.float32)
    do_cache(data, N, K)

if __name__ == "__main__":
    main()
