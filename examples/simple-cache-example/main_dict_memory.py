from simple_caching.storage import DictMemory
import numpy as np
from datetime import datetime
import time
import random

data = np.random.randn(100, 420, 420, 3).astype(np.float32)

def key_encode_fn(x):
    return f"{x.mean()}_{x.std()}"

def f(x):
    if random.random() < 0.2:
        time.sleep(0.1)
    return (x ** 2 / np.sqrt(x)) if np.sqrt(x).mean() > 2.2 else x * 0

class Timer:
    def __init__(self, message: str = None, end_message: str = None):
        if message is not None:
            print(message)
        if end_message is None:
            end_message = ""
        self.end_message = end_message

    def __enter__(self):
        self.now = datetime.now()

    def __exit__(self, a , b, c):
        took = datetime.now() - self.now
        print(f"{self.end_message} {took}")

def access(cache, data):
    with Timer(end_message="Regular access time. "):
        for i in range(len(data)):
            item = f(data[i])

    with Timer(end_message="Memory FS Cache. "):
        for i in range(len(data)):
            item = cache[data[i]]

def cache_manually():
    cache = DictMemory("manually", key_encode_fn=key_encode_fn)
    with Timer(end_message="Populate timer #1 "):
        for item in data:
            cache[item] = f(item)
    with Timer(end_message="Populate timer #2 "):
        for item in data:
            cache[item] = f(item)
    access(cache, data)

def cache_manually_check():
    cache = DictMemory("manually + check", key_encode_fn=key_encode_fn)
    with Timer(end_message="Populate timer #1 "):
        for item in data:
            cache[item] = f(item)
    with Timer(end_message="Populate timer #2 "):
        for item in data:
            if not item in cache:
                cache[item] = f(item)
    access(cache, data)

def cache_map():
    cache = DictMemory("map", key_encode_fn=key_encode_fn)
    with Timer(end_message="Populate timer #1 "):
        cache.map(f, data)
    with Timer(end_message="Populate timer #2 "):
        cache.map(f, data)
    access(cache, data)

def main():
    with Timer("== Manually ==", end_message="----> Total "):
        cache_manually()
    with Timer("== Manually + check ==", end_message="----> Total "):
        cache_manually_check()
    with Timer("== Map ==", end_message="----> Total "):
        cache_map()

if __name__ == "__main__":
    main()
