"""Utility functions module"""
# pylint: disable=invalid-name
from typing import TypeVar, Callable
from pathlib import Path
import os
import hashlib
import numpy as np

KeyType = TypeVar("KeyType")
EncodedKeyType = TypeVar("EncodedKeyType")
ValueType = TypeVar("ValueType")
ValueFnType = Callable[[KeyType], ValueType]
KeyEncodeFnType = Callable[[KeyType], EncodedKeyType]

try:
    DEFAULT_CACHE_DIR = Path(os.environ["SIMPLE_CACHING_DIR"])
except KeyError:
    # If the env variable is not set, we use the root dir of this project
    DEFAULT_CACHE_DIR = Path(__file__).absolute().parents[1] / ".cache"

def identity_encode_fn(x: KeyType) -> EncodedKeyType:
    """Identity function"""
    return x

def np_encode_fn(x: np.ndarray) -> str:
    """default encoding for a numerical numpy array"""
    # TODO: support for object/string arrays
    # TODO: what is std() returns nan?
    return f"{x.shape}_{x.mean():.5f}_{x.std():.5f}"

def auto_encode_fn(*args) -> str:
    """default encoding for a general function with some supported data types"""
    res = ""
    for arg in args:
        if isinstance(arg, np.ndarray):
            res += np_encode_fn(arg)
        if isinstance(arg, str):
            res += arg
        if isinstance(arg, (int, float)):
            res += str(arg)
    res_hash = hashlib.md5(res.encode("utf-8")).hexdigest()
    return res_hash
