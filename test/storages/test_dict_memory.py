from simple_caching.storage import DictMemory

class TestDictMemory:
    def test_memorydict_1(self):
        cache = DictMemory("test")
        assert not cache is None

    def test_memorydict_2(self):
        cache = DictMemory("test")
        assert not "hi" in cache
        try:
            _ = cache["hi"]
            assert False
        except Exception:
            pass
        cache["hi"] = 3
        assert "hi" in cache
        assert cache["hi"] == 3
