import pytest
import shutil
from simple_caching.storage import NpyFS
from pathlib import Path

@pytest.fixture(autouse=True)
def run_around_tests():
    try:
        shutil.rmtree(Path(__file__).parent / "test/")
    except:
        pass
    yield

class TestNpyFS:
    def test_npyfs_1(self):
        cache = NpyFS("test")
        assert not cache is None

    def test_npyfs_2(self):
        cache = NpyFS("test")
        assert not "hi" in cache
        try:
            _ = cache["hi"]
            assert False
        except Exception:
            pass
        cache["hi"] = 3
        assert "hi" in cache
        assert cache["hi"] == 3
